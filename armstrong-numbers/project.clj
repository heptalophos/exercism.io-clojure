(defproject armstrong-numbers "0.1.0-SNAPSHOT"
  :description "armstrong-numbers exercise"
  :url "https://github.com/exercism/clojure/tree/master/exercises/armstrong-numbers"
  :dependencies [[org.clojure/clojure "1.8.0"]]
  ; :main ^:skip-aot armstrong-numbers
  ; :target-path "target/%s"
  ; :profiles {:dev {:dependencies [[midje "1.7.0"]]}
  ;                  :plugins [[lein-midje "3.2.1"]]         
  ;                  :uberjar {:aot :all}}
)
